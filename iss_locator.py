#!/bin/usr/python3

### 1. List people currently in space ###
### 2. Provide a map with the current location of the ISS ###
### 3. Find the next time the ISS will be passing over Tallahassee, FL ###

import json               
import turtle
import requests
import urllib.request
import time

print()
print('***Welcome to ISS Locator!***')
print()

# open JSON file crew information
url = 'http://api.open-notify.org/astros.json'
response = urllib.request.urlopen(url)
result = json.loads(response.read())

# print ISS crew information from JSON file
print('Number of people currenty in space:', result['number'])
people = result['people']
for each_person in people:
    print('-', each_person['name'],'aboard', each_person['craft'])

# open JSON file with ISS coordinate information
url = 'http://api.open-notify.org/iss-now.json'
response = urllib.request.urlopen(url)
result = json.loads(response.read())

# retrieve ISS locatio information from JSON file
location = result['iss_position']
lat = float(location['latitude'])
lon = float(location['longitude'])
print('\nCurrent ISS location:')
print('Latitude: ', lat)
print('Longitude: ', lon)

# display map of earth
screen = turtle.Screen()
screen.setup(720, 360)
screen.setworldcoordinates(-180, -90, 180, 90)
screen.bgpic("/Users/mtdemaria/.ssh/LearnPython/spacestation/map.gif")

# create ISS Turtle 
screen.register_shape("/Users/mtdemaria/.ssh/LearnPython/spacestation/iss.gif")
iss = turtle.Turtle()
iss.shape("/Users/mtdemaria/.ssh/LearnPython/spacestation/iss.gif")
iss.setheading(90)

iss.penup()
iss.goto(lon, lat)

# When will the ISS next be overhead?

# Tallahassee, FL coordinates:
lat = 30.455109
lon = -84.253419


# Create a yellow dot that represents Tallahassee, FL
location_TLH = turtle.Turtle()
location_TLH.penup()
location_TLH.color('yellow')
location_TLH.goto(lon, lat)
location_TLH.dot(5)
location_TLH.hideturtle()

# determine when ISS will pass above Tallahassee, FL
url = 'http://api.open-notify.org/iss-pass.json'
url = url + '?lat=' + str(lat) + '&lon=' + str(lon)
response = urllib.request.urlopen(url)
result = json.loads(response.read())

over_TLH = result['response'][1]['risetime']

# print time and date of next ISS sighting over Tallahassee, FL
style = ('Arial', 12, 'bold')
location_TLH.write(time.ctime(over_TLH), font=style)

print()
print('Thank you for using ISS Locator!')
print('(Click the map to exit)')
print()

# click the map to quit
screen.exitonclick()

        

